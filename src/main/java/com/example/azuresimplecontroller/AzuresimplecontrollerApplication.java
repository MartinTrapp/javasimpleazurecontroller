package com.example.azuresimplecontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzuresimplecontrollerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AzuresimplecontrollerApplication.class, args);
	}

}
